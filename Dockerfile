FROM maven:3.6-jdk-8-alpine AS builder
VOLUME /tmp
ADD ./target/exam-service-person-0.0.1-SNAPSHOT.jar service-person.jar
ENTRYPOINT ["java","-jar","/service-person.jar"]