package com.finalexam.trujillo.app.person;

import com.finalexam.trujillo.app.person.model.entity.Person;
import com.finalexam.trujillo.app.person.repository.PersonRepository;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@SpringBootApplication
public class ExamServicePersonApplication implements CommandLineRunner {

    @Autowired
    private PersonRepository personRepository;

    public static void main(String[] args) throws UnknownHostException {

       // SpringApplication.run(ExamServicePersonApplication.class, args);

        Environment env = SpringApplication.run(ExamServicePersonApplication.class, args).getEnvironment();
        log.info("\n----------------------------------------------------------\n\t"
                        + "Application '{}' is running! Access URLs:\n\t"
                        + "Local: \t\thttp://localhost:{}\n\t"
                        + "External: \t\thttp://{}:{}\n\t"
                        + "DB: \t{}\n\t"
                        + "Profile(s): \t{}\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                env.getProperty("spring.data.mongodb.database"),
                env.getActiveProfiles());
        String configServerStatus = env.getProperty("configserver.status");
        log.info("\n----------------------------------------------------------\n\t"
                        + "Config Server: \t{}\n----------------------------------------------------------",
                configServerStatus == null ? "Not found or not setup for this application" : configServerStatus);


    }

    @Override
    public void run(String... args) throws Exception {
        personRepository.deleteAll().subscribe();
        Flowable<Person> person = Flowable.fromArray(Person.builder()
                        .document("10000000")
                        .fingerprint(true)
                        .blacklist(false)
                        .build(),
                Person.builder()
                        .document("10000001")
                        .fingerprint(false)
                        .blacklist(false)
                        .build(),
                Person.builder()
                        .document("10000002")
                        .fingerprint(true)
                        .blacklist(true)
                        .build());

        personRepository.saveAll(person)
                .subscribe();
    }

}
