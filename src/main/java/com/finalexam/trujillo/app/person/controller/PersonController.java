package com.finalexam.trujillo.app.person.controller;


import com.finalexam.trujillo.app.person.model.entity.Person;
import com.finalexam.trujillo.app.person.service.impl.IPersonService;
import io.reactivex.Flowable;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/core")
public class PersonController {

    @Autowired
    private IPersonService iPersonService;

    @GetMapping(value = "/persons/{documentNumber}")
    public Single<Person> getPerson(@PathVariable String documentNumber) {

        return iPersonService.findByDocument(documentNumber);

    }

    @GetMapping(value = "/list")
    public Flowable<Person> list() {

        return iPersonService.listAll();

    }


}
