package com.finalexam.trujillo.app.person.model.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document
public class Person {

    @Id
    private String id;
    @Field(name = "Document")
    private String document;
    @Field(name = "Fingerprint")
    private Boolean fingerprint;
    @Field(name = "Blacklist")
    private Boolean blacklist;


}
