package com.finalexam.trujillo.app.person.repository;

import com.finalexam.trujillo.app.person.model.entity.Person;
import io.reactivex.Maybe;
import org.springframework.data.repository.reactive.RxJava2CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends RxJava2CrudRepository<Person,String> {

    Maybe<Person> findByDocument(String document);


}

