package com.finalexam.trujillo.app.person.service.impl;

import com.finalexam.trujillo.app.person.model.entity.Person;
import io.reactivex.Flowable;
import io.reactivex.Single;

public interface IPersonService {

    Single<Person> findByDocument(String document);


    Flowable<Person> listAll();


}
