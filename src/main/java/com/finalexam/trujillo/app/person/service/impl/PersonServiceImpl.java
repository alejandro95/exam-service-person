package com.finalexam.trujillo.app.person.service.impl;

import com.finalexam.trujillo.app.person.model.entity.Person;
import com.finalexam.trujillo.app.person.repository.PersonRepository;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonServiceImpl implements IPersonService {
    @Autowired
    private PersonRepository personRepository;

    @Override
    public Single<Person> findByDocument(String document) {

        return personRepository.findByDocument(document)
                .switchIfEmpty(Maybe.error(NullPointerException::new)).toSingle();




    }

    @Override
    public Flowable<Person> listAll() {

        return personRepository.findAll();
    }
}